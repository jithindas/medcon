import '../navbar/navbar.css'
import Logo  from '../images/one.png'
import { FaSistrix } from 'react-icons/fa';

function Navbar() {
  return (
    <nav className='main_Nav'>
      <div className='SubNav'>
          <div className='logo'>
          <img src={Logo} width= '150'/>
          </div>
      </div>
      <div className='Navlinks'>
        <div><text className='linktext'>Homepage</text></div>
        <div><text>Webinar</text></div>
        <div><text>Webinar</text></div>
      </div>
      <div className='login'>
        <div className='bc'>
          <div className='b1'>
            <button className='btn'>
              <text className='sign'>SIGN IN</text>
            </button>
          </div>
          <div className='b1'>
            <button className='btn1'>
              <text className='sign'>REGISTER</text>
            </button>
          </div>
        </div>
        <div className='input'>
          <div className='inputz'>
            <input className='putz' placeholder='What are you looking for?'></input>
          </div>
        </div>
        <div className='search'><FaSistrix /></div>
      </div>
    </nav>
  );
}

export default Navbar;
