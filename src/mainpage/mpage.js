import body from '../images/two.png'
import side from '../images/Mask Group 6.png'
import rside from '../images/Group 716.png'
function Mpage() {
  return (
      <div>
          <div className='side'>
          <img src={body} width= '100%'/>
          </div>
          <div className='tside'>
            <text className='hdr'>Evolving your knowledge in 
diabetes at a different beat</text>
          </div>
          <div className='side1' >
          <img src={side} width= '90%'/>
          </div>
          <div className='rside' >
          <img src={rside} width= '70%'/>
          </div>
      </div>
  );
}

export default Mpage;
