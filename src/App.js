import logo from './logo.svg';
import './App.css';
import Navbar from '../src/navbar/navbar';
import Mpage from './mainpage/mpage';
import Videocon from './videotab/videocon';

function App() {
  return (
    <div className='main'>
      <Navbar/>
      <Mpage/>
      {/* <Videocon/> */}
    </div>
  );
}

export default App;
